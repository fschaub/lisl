{-# LANGUAGE StandaloneDeriving #-}
module Main 
where

import Control.Monad
import LiSL



-- * Setup

-- ** Datatype for Dozer Variables

-- | the datatype includes information about the type of
-- the variables.
data DozerVariable a where
  PXFollowUpContEnabled :: DozerVariable Bool 
  SXWiperIntFrontUp     :: DozerVariable Bool
  SDWiperIntFront       :: DozerVariable Int
  SRDummyEnabled        :: DozerVariable Float


-- ** Make a Parameter instance

-- use haskell "magic" to generate `Show` instance
deriving instance Show (DozerVariable a)
-- now `show PXFollowUpContEnabled = "PXFollowUpContEnabled"` 

-- derive `Parameter` instance by Show instance
instance Parameter DozerVariable where
  paramKey = show


-- * Stimulations

-- | reactive stimulation example
exampleStimulation :: Stimulation 'Reactive ()
exampleStimulation = do
  -- #########################################################
  -- Creating Effects
  -- #########################################################
  -- effects are either
  --    - Apply a Force to the Target
  --    - Modified the current effect
  --    - NoEffect
  --
  -- create an Apply effect by forcing a DozerVariable strictly
  -- (strict force: the effect is kept until it is overwritten)
  force Strict PXFollowUpContEnabled True 
  -- or use `strict`
  strict PXFollowUpContEnabled True
  -- we could also have forced the DozerVariable lazily
  -- (lazy force: the effect is only temporary)
  force Lazy PXFollowUpContEnabled True
  -- or 
  lazy PXFollowUpContEnabled True
  --
  -- forces are also possible with sequences of values
  lazy PXFollowUpContEnabled [False, True, False]
  -- we can also use `toggle` here
  toggle PXFollowUpContEnabled
  -- 
  -- we create several empty Apply effects to wait.
  wait 5
  -- 
  -- we can modify the current effect by creating a Modified effect.
  -- unforce cancels the held effect on the DozerVariable
  unforce PXFollowUpContEnabled
  --
  -- for completenes we can also create stimulations that 
  -- have no effect.
  -- for example `skip` (does nothing)
  skip
  --
  --
  --
  --
  -- #########################################################
  -- Combinations
  -- #########################################################
  -- sequential combination is just the monadic sequence (as above)
  -- parallel combination is done with `parallel`
  parallel  
    [ -- we can force Foldables (Lists or such)
      strict  PXFollowUpContEnabled [False, True, False]
    , -- we can apply functions to the lists
      -- here f(x) = x² over the x-domain {0,1,2,3,4,5,6,7,8,9,10}
      strict  SDWiperIntFront (map (^2) [0 .. 10])
    ]
  unforce PXFollowUpContEnabled
  --
  -- preemptive combination is via `preemptive`
  preemptive  
    [ -- we can force Foldables (Lists or such)
      strict  PXFollowUpContEnabled [False, True, False]
    , -- we can apply functions to the lists
      -- here f(x) = x² over the x-domain {0,1,2,3,4,5,6,7,8,9,10}
      strict  SDWiperIntFront (map (^2) [0 .. 10])
    ]
  unforce PXFollowUpContEnabled
  -- 
  -- we have "for loops" (not really 😉)
  -- for each element `i` in [0 .. 1000] we perform a stimulation
  forM_ [0 .. 1000] $ \i -> do
    -- this is the reactive part
    -- get the value of SDWiperIntFront at the current state
    sdwiperintfrontValue <- get SDWiperIntFront
    -- define a ramp
    let sdwiperintfrontRamp = map (sdwiperintfrontValue + 1 +) [0 .. i]
    -- apply the ramp
    strict  SDWiperIntFront sdwiperintfrontRamp
    -- reset SDWiperIntFront
    strict  SDWiperIntFront sdwiperintfrontValue
    -- lazy sets AND unforces SDWiperIntFront
    lazy    SDWiperIntFront 1
  --
  --
  -- #########################################################
  -- About Strong Static (Dependent) Typing
  -- #########################################################
  -- we want to be shure that stimulations don't
  -- force nonsense values. The strong type scheme of haskell allows
  -- us to encode these restrictions.
  -- won't run:
  --    - lazy SRDummyEnabled True
  --    - lazy SDWiperIntFront True
  --    - lazy SDWiperIntFront 1.1
  --    - lazy PXFollowUpContEnabled 1
  --    - lazy SRDummyEnabled (lazy SDWiperIntFront)
  -- will run:
  --    - lazy SRDummyEnabled 1.5
  --    - lazy SRDummyEnabled (1 :: Int)
  --    - lazy SDWiperIntFront 2
  --    - lazy PXFollowUpContEnabled True
  --
  -- furthermore, we want to check this beforehand
  -- and not after a stimulation already ran 1 hour.
  -- Static typing of haskell tells us this at compile time
  --    - examples above don't compile if they won't run
  -- 
  -- finally, haskell's typing scheme 
  --    - strong
  --    - static
  --    - real polymophism
  --    - higher-order types
  -- allows us to have static type safety inside the haskell part
  --    - no runtime checks, only errors (caused by outside code)
  --    - no unnessacery casting/coercing of values in stimulations
  -- although the foreign code (LCF and Bindings; in python)
  -- relies heavily on dynamic typing with runtime checks.
  --
  --
  -- The type scheme also allows us to strictly define
  -- if we want to allow reactive or static stimulations
  -- at certain points. Although static and reactive stimulations
  -- share the same implementation, they are different types
  -- whose relation is explicitly defined. 
  -- This makes them only compatible in a clear and sound way.

-- | static stimulation example
exampleStimulation2 :: Stimulation 'Static ()
exampleStimulation2 = do
  -- #########################################################
  -- Everithing as usual....
  -- #########################################################
  -- define a ramp
  let xDom = [0.2, 0.25 .. 0.8]   -- the x-domain
      rampUpF x = x * 0.3 + 22.1  -- the function
      rampUp = map rampUpF xDom   -- apply the function to all elements in the x-domain
  -- apply a stimulation 10 times
  replicateM_ 10
    -- lazily force SDWiperIntFront to the sequence 0 1 2 3    
    $ lazy SDWiperIntFront [0,1,2,3]
  --
  -- #########################################################
  -- ... but no reactiveness
  -- #########################################################
  -- if we uncomment the line below the stimulation won't compile
  -- _ <- get SDWiperIntFront
  --
  --
  -- #########################################################
  -- More on Computability
  -- #########################################################
  -- apply the stimulation 12346 times
  replicateM_ 12346 
    -- preemptively combine stimulations
    -- the shortest one terminates the combination
    $ preemptive
      [ -- recursive application of exampleStimulation2
        -- without preemptive this would clearly not terminate
        exampleStimulation2
      , -- forever never terminates
        forever $ toggle PXFollowUpContEnabled
      , -- apply the ramp lazily (terminates)
        lazy SRDummyEnabled rampUp
      ]







-- | dummy main
main :: IO ()
main = do
  runStatic exampleStimulation2 $ \st -> print st >> return st
