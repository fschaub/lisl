module TestCases 
where

import LiSL
import Main


-- | construct a testcase from a stimulation
-- this is needed to use it from the python side
tc01_example :: TestCase
tc01_example = testcase exampleStimulation2

-- | the testcase is made available to python 
foreign export ccall tc01_example :: TestCase
