{-
  Block comment 
-}

-- line comment

-- #########################################################
-- Preamble
-- #########################################################

-- Haskell files are modules that give the contents qualified names
module HSPrimer where

-- use other modules with the import statement
import GHC.IO
-- we may only import certain functions
import Control.Monad (forever)
-- we may hide certain functions
import Data.Bifunctor hiding (bimap)
-- we may import something qualified
import qualified Data.HashMap.Strict as Map



-- #########################################################
-- Functions
-- #########################################################
{- 
haskell is a pure functional programming language with
static strong typing
  
  - functional: functions are the main building blocks
                and may be used like all other "variables".
  
  - pure:       there are no sideeffects: everything is
                constant or inside a monadic context
                augmenting sideeffects.
  
  - static:     everything regarding typing is checked 
                at compiletime (without running the program)
  
  - strong:     there are no implicit type conversions.

Like most functional languages in haskell function application
is written as juxtaposition (writing side by side):
  Python:   print(x)
  Haskell:  print x

the corresponding function to a python function with more arguments are
haskell functions that take a tuple:
  Python:   exp(b,e)
  Haskell:  exp (b,e)

Yet in FP we (mostly) don't do this. We use currying:
  Python:   foo(b)(e)
  Haskell:  foo b e

Function application in haskell is left associative:
  Python:   pow(b, root(x,y))
  Haskell:  pow b (root x y)

There is the convenience operator `$`:
  Python:   pow(b, root(x,y))
  Haskell:  pow b $ root x y


We can create definitions (you can just think of them as constant variables)
with the `let .. in ..` syntax
  Python:
            e = root(x,y)
            r = pow(b,e)
            return r
  Haskell:  
            let
              e = root x y
              r = pow b e
            in r  

Haskell's equality is a definitional equality:
  Haskell:
            x = x + 1
  Python:
            def x():
              return x() + 1

-}


-- #########################################################
-- Do-Notation
-- #########################################################
{- 
  As mentioned we need monadic contexts to model sideeffects.
  for this we have the Do-Notation. 


  Functions in this notation are implicitly combined 
  with (>>=) and (>>).

  do { foo ; bar }          is desugared to foo >> bar
  do { x <- foo ; bar x }   is desugared to foo >>= \x -> bar x
-}

-- we start a function in the context of the IO Monad
exampleDoFunction b x y = do
  -- `let` expressions loose the `in`
  let pow x y   = x ** y
      root x y  = pow x (1/y)
      e = root x y
      r = pow b e
  -- we have functions that operate on the context
  print r
  -- we get values from the context with `<-`
  line <- getLine
  putStrLn $ show line ++ " yourself!!!!"
  -- we lift a result value into the context by using `return` or `pure`
  return r




