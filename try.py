
from ctypes import *
import os 
import sys


def encode(d: dict):
  acc = ""
  for k in d:
    acc += f"{k}\t{type(d)}\t{d[k]}\n"
  return c_char_p(bytes(acc, 'utf8'))

def decode(cs: c_char_p):
  def part(state: str, delim: str, start: int):
    offset = 0
    while state[start + offset] != delim:
      offset += 1
    ret = state[start : start + offset]
    return (ret, start + offset + 1)
  decoded = {}
  state = str(cs, 'utf8')
  curs = 0
  while curs < len(state):
    par, curs = part(state, '\t', curs) 
    dtype, curs = part(state, '\t', curs)
    rawval, curs = part(state, '\n', curs)
    if dtype == "float":
      decoded[par] = float(rawval)
    elif dtype == "int":
      decoded[par] = int(rawval)
    elif dtype == "bool":
      decoded[par] = bool(rawval)
    else:
      raise Exception(f"type error : ''{par}'' ''{dtype}''")
  return decoded


BUFFER = pointer(c_char_p(b'initial buffer'))
TARGET = None




@CFUNCTYPE(None, c_char_p, POINTER(c_char_p))
def targetConnector(state: c_char_p, ret: POINTER(c_char_p)) -> None:
  try:
    nstate = TARGET(decode(state))
    ret[0] = encode(nstate)
  except BrokenPipeError:
    devnull = os.open(os.devnull, os.O_WRONLY)
    os.dup2(devnull, sys.stdout.fileno())


class LiSLlib(object):
  def __init__(self, lib):
    self.testcases = lib
    self.testcases.hs_init(None, None)
  def __del__(self):
    self.testcases.hs_exit()
  
def getLiSLlib(name: str) -> LiSLlib:
  cdll.LoadLibrary(name)
  return LiSLlib(CDLL(name))


def runTestCase(tc, target):
  global TARGET
  TARGET = target  
  tc(BUFFER, targetConnector)
  TARGET = None
  

def dummytarget(state):
  print(state)
  return state


exampleLib = getLiSLlib("CLiSL.so")
runTestCase(exampleLib.testcases.tc01_example, dummytarget)










