module LiSL.Types 
  ( -- * Base Types
    Value(..)
  , Force(..)
  , Record
  , -- * Targets
    TargetState
  , Target
  , -- * Stimulations
    Effect(..)
  , StimType(..)
  , type (⊆)
  , Stimulation(..)
  )
where


import qualified Data.HashMap.Strict as Map
import Control.Arrow (second)
import Data.Kind (Type)

-- | a wrapper type to allow us to use simple HashMaps with
-- differently typed parameters
data Value = Int !Int | Bool !Bool | Float !Float 
  deriving(Show, Eq)

-- | a force denotes either to
-- set a parameter to a value for one time step (`Lazy`) or to
-- set a parameter to a value indefinetly long (`Strict`)
data Force = Strict Value | Lazy Value
  deriving (Show, Eq)

-- | a record maps `LiSL.Parameter.Parameter` names to `Force`s
type Record = Map.HashMap String Force

-- | the targetstate maps `LiSL.Parameter.Parameter` names to `Values`s
-- they either have or should take.
type TargetState = Map.HashMap String Value

-- | a target of Monad @m@ is a function from a `TargetState` (the forcing values) 
-- to a `TargetState` (the state after forcing)
type Target (m :: Type -> Type) = TargetState -> m TargetState

-- | an effect denotes the sideeffect of the `Stimulation`.
data Effect 
  = Apply Record    -- ^ apply the record to the target
  | Modified Record -- ^ the current record has been modified
  | NoEffect        -- ^ no effect was generated


-- | the Kind of the `Stimulation`.  
data StimType 
  = Static      -- ^ static stimulations cannot depend on the `TargetState` during execution. 
  | Reactive    -- ^ reactive stimulations have access to the `TargetState` during execution.

-- | a containment relation for `StimType`s. 
class (⊆) (a :: StimType) (b :: StimType)
instance 'Static   ⊆  'Static
instance 'Static   ⊆  'Reactive
instance 'Reactive ⊆  'Reactive

-- | the Stimulation is a monadic type that generates `Effect`s to manipulate a `Target`. 
-- Instances of the type are combined sequentially or parallelly. The `StimType` annotation
-- restricts the Stimulation to be either `Static` or `Reactive`.
data Stimulation (t :: StimType) a where
  Seq     :: Stimulation t b -> (b -> Stimulation t a) -> Stimulation t a   -- ^ sequence two stimulations
  Par     :: Stimulation t a -> Stimulation t a -> Stimulation t a          -- ^ parallelly combine two stimulations. both are evaluated to termination.
  Preempt :: Stimulation t a -> Stimulation t a -> Stimulation t a          -- ^ parallelly combine two stimulations. the shorter one causes termination.
  Gen     :: (TargetState -> Record -> (Effect, a)) -> Stimulation t a      -- ^ generate an `Effect` and a return value from the `TargetState` and `Record`
  Term    :: a -> Stimulation t a                                           -- ^ terminated stimulation that yields a value

instance Functor (Stimulation t) where
  fmap f st = case st of
    (Seq x y)       -> Seq x $ \v -> fmap f (y v)
    (Par x y)       -> Par (fmap f x) (fmap f y)
    (Preempt x y)   -> Preempt (fmap f x) (fmap f y)
    (Gen g)         -> Gen $ \ts r -> second f $ g ts r 
    (Term x)        -> Term (f x)
instance Applicative (Stimulation t) where
  pure    = Term
  x <*> y = Seq x (<$> y)
instance Monad (Stimulation t) where
  return  = pure
  x >>= f = Seq x f

