module LiSL.Run
  ( runStatic
  , runReactive
  )
where
  
import LiSL.Evaluate
import LiSL.Types


-- | run a static Stimulation on a Target.
--
-- the @t ⊆ `'Static`@ constraint guarantees that the evaluated stimulation is 
-- static.
runStatic :: (Monad m, t ⊆ 'Static) => Stimulation t a -> Target m -> m a
runStatic = evaluate

-- | run a reactive Stimulation on a Target.
--
-- the @t ⊆ `'Reactive`@ constraint allows reactive stimulations
runReactive :: (Monad m, t ⊆ 'Reactive) => Stimulation t a -> Target m -> m a
runReactive = evaluate



