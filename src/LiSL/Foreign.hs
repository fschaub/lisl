module LiSL.Foreign 
  ( testcase
  , TestCase
  , ForeignTarget
  )
where

import Foreign.C.String
import Foreign.Ptr
import Foreign hiding (void)
import Control.Monad (void)
import qualified Data.HashMap.Strict as Map


import LiSL.Types 
import LiSL.Run

-- | a target for the FFI is a function that takes
-- a `CString` (the encoded forcing `TargetState`),
-- a pointer to a `CString` (where to write the resulting `TargetState`)
-- and performs an IO action.
type ForeignTarget = CString -> Ptr CString -> IO ()

-- | a testcase for the FFI takes
-- a pointer to a CString (the buffer managed by the foreign code),
-- a function pointer to the foreign target
-- and performs and IO action
type TestCase = Ptr CString -> FunPtr ForeignTarget -> IO ()


-- | the function to cast a functionpointer to a foreign target to a haskell function
--
-- this can be unsafe, since no haskell heap data is involved
foreign import ccall unsafe "dynamic" mkTarget :: FunPtr ForeignTarget -> ForeignTarget

-- | wrapper class to allow to construct `TestCase`s from Static and Reactive Stimulations
class TC (t :: StimType) where
  testcase :: Stimulation t a -> TestCase
instance TC 'Static where
  testcase stim buf target = do
    runStatic (void stim) (nativeTarget buf $ mkTarget target)
instance TC 'Reactive where
  testcase stim buf target = do
    runReactive (void stim) (nativeTarget buf $ mkTarget target)

-- | convert a `ForeignTarget` to a native target
nativeTarget :: Ptr CString -> ForeignTarget -> Target IO
nativeTarget buf fun st = withCString (encode st) $ \cst -> do
  fun cst buf
  fmap decode . peekCString =<< peek buf

{- * TargetState encoding

TargetStates encoded as CStrings for communication with foreign targets.
The format is kept as simple as possible:

for each entry in the TargetState we create a String:
  (key, Int val)    -> "key\\tint\\tval"
  (key, Float val)  -> "key\\tfloat\\tval"
  (key, Bool val)   -> "key\\tbool\\tval"
the entries are then concatenated with the newline character ’\\n’
  
-}


-- | encode a `TargetState`
encode :: TargetState -> String
encode = Map.foldlWithKey' enc ""
  where
    enc acc p (Int v)   = p ++ "\tint\t"    ++ show v ++ "\n" ++ acc
    enc acc p (Float v) = p ++ "\tfloat\t"  ++ show v ++ "\n" ++ acc
    enc acc p (Bool v)  = p ++ "\tbool\t"   ++ show v ++ "\n" ++ acc

-- | decode a `TargetState`.
--
-- this is held lazy on purpose, since the result state isn't 
-- used in most cycles.
decode :: String -> TargetState
decode = Map.fromList . map dec . lines
  where
    dec s = 
      let
        (par    , s1) = part s
        (dtype  , s2) = part s1
        val           = case dtype of
          "int"   -> Int    . read  $ getVal s2
          "float" -> Float  . read  $ getVal s2
          "bool"  -> Bool   . read  $ getVal s2
          _       -> error "invalid type entry"
      in
        (par, val)
    part ('\t':xs)  = ("", xs)
    part (x   :xs)  = let (y, ys) = part xs in (x:y, ys)
    part []         = error "cannot parse empty string"
    getVal ('\n':_)   = ""
    getVal (x   :xs)  = x : getVal xs
    getVal []         = error "cannot parse empty string"
    