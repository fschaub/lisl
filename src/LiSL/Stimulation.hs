{- | 

@LiSL.Stimulations@ are monadically composable functions that 
generate stimulations. Each `>>=` composes two stimulations
sequencially and the functions `parallel` and `preemptive` compose stimulations
parallelly. Since not every `Stimulation` instance has to generate a stimulation-`Effect` you
can embed your stimulation in arbitrary computations.

-}

module LiSL.Stimulation 
  ( -- * Combinators
    skip
  , (|||)
  , parallel
  , preemptive
  , -- * Static Stimulations
    Forceable
  , force
  , lazy
  , strict
  , toggle
  , unforce
  , wait
  , -- * Reactive Stimulations
    get
  , fromStatic
  )
where

import qualified Data.HashMap.Strict as Map
import Data.Foldable (foldl')

import LiSL.Types 
import LiSL.Parameter



-- | an empty stimulation that neither produces an `Effect` nor returns a value.
skip :: Stimulation t ()
skip = pure ()

-- | infix parallel combinator of Stimulations. 
-- The stimulations are run until all are terminated.
(|||) :: Stimulation t () -> Stimulation t () -> Stimulation t ()
(|||) = Par 
infixl 2 |||

-- | parallelly compose all stimulations in the list. 
-- The stimulations are run until all are terminated.
parallel :: [Stimulation t ()] -> Stimulation t ()
parallel []     = skip 
parallel (x:xs) = foldl' Par x xs


-- | parallelly compose the stimulations in the list. 
-- The stimulations are run untils one has terminated
preemptive :: [Stimulation t ()] -> Stimulation t ()
preemptive []      = skip
preemptive (x:xs)  = foldl' Preempt x xs




-- | force the parameter to the given value with the constuctor given (`Lazy`, `Strict`)
forceImpl :: (Pack a, 'Static ⊆ t, Parameter p) => p a -> a -> (Value -> Force) -> Stimulation t ()
forceImpl p v c = Gen $ \_ _ -> 
    let r = Map.singleton (paramKey p) (c $ pack p v)
    in (Apply r, ())

-- | type class to implement forcing with different types or `Foldable`s 
class Forceable a b where
  forcefun :: ('Static ⊆ t, Parameter p) => p a -> b -> (Value -> Force) -> Stimulation t ()
instance Forceable Int Int where
  forcefun = forceImpl
instance Forceable Int Integer where
  forcefun p v = forceImpl p (fromIntegral v :: Int)
instance Forceable Float Float where
  forcefun = forceImpl
instance Forceable Float Int where
  forcefun p v = forceImpl p (realToFrac v :: Float)
instance Forceable Float Integer where
  forcefun p v = forceImpl p (realToFrac v :: Float)
instance Forceable Float Double where
  forcefun p v = forceImpl p (realToFrac v :: Float)
instance Forceable Bool Bool where
  forcefun = forceImpl

instance (Pack a, Forceable a b, Foldable f) => Forceable a (f b) where
  forcefun p xs c = foldl' (\acc x -> acc >> forcefun p x c) skip xs

-- | force the parameter to the value with the given constructor (`Lazy`, `Strict`)
force :: (Forceable a b, 'Static ⊆ t, Parameter p) => (Value -> Force) -> p a -> b -> Stimulation t ()
force c p v = forcefun p v c
-- | lazily force the parameter to the value
lazy  :: (Forceable a b, 'Static ⊆ t, Parameter p) => p a -> b -> Stimulation t ()
lazy = force Lazy
-- | strictly force the parameter to the value
strict :: (Forceable a b, 'Static ⊆ t, Parameter p) => p a -> b -> Stimulation t ()
strict = force Strict

class Toggle b where
  toggle :: (Forceable b b, 'Static ⊆ t, Parameter p) => p b -> Stimulation t ()
instance Toggle Int where
  toggle p = lazy p [0,1,0]
instance Toggle Bool where
  toggle p = lazy p [False, True, False]

-- | unforce the given parameter
unforce :: Parameter p => p a -> Stimulation t ()
unforce p = Gen $ \_ r ->
  let r' = Map.delete (paramKey p) r
  in (Modified r', ())

-- | wait for @n@ steps without forcing anything.
wait :: Int -> Stimulation t ()
wait n 
  | n <= 0    = skip
  | otherwise = 
    let
      empty = Gen $ \_ _ -> (Apply Map.empty, ())
    in 
      empty >> wait (n - 1) 

-- | get the current `TargetState`
getState :: 'Reactive ⊆ t => Stimulation t TargetState
getState = Gen $ \ts _ -> (NoEffect, ts)

-- | get the value of a `Parameter`
get :: ('Reactive ⊆ t, Pack a, Parameter p) => p a -> Stimulation t a
get p = unpack p . (Map.! paramKey p) <$> getState
  

-- | convert a static stimulation to a reactive stimulation
fromStatic :: Stimulation 'Static a -> Stimulation 'Reactive a
fromStatic (Seq x y)      = Seq (fromStatic x) (fromStatic . y)
fromStatic (Par x y)      = Par (fromStatic x) (fromStatic y)
fromStatic (Preempt x y)  = Preempt (fromStatic x) (fromStatic y)
fromStatic (Gen g)        = Gen g
fromStatic (Term v)       = Term v