
module LiSL.Evaluate 
  ( evaluate
  )
where

import LiSL.Types 
import LiSL.Record


-- | evaluate a stimulation on a target. this function is not to be
-- used directly. use `LiSL.Run.runStatic` and `LiSL.Run.runReactive` instead. 
evaluate :: Monad m => Stimulation t a -> Target m -> m a 
evaluate stim target = do
  state <- target mempty
  go stim state mempty
  where
    go (Term x) _ _       = pure x 
    go stim state record  = 
      let 
        (stim', effect) = unfold stim state record
      in 
        case effect of
          NoEffect            -> go stim' state record
          (Modified record' ) -> go stim' state record'
          (Apply    record' ) -> do
            let fullrecord = carry record record'
            state' <- target $ toTargetState fullrecord
            go stim' state' fullrecord
    unfold :: Stimulation t a -> TargetState -> Record -> (Stimulation t a, Effect)
    unfold (Term x) _     _      = (Term x, NoEffect)
    unfold (Gen g)  state record = 
      let 
        (effect, x) = g state record
      in 
        (Term x, effect)
    unfold (Seq x y) state record = case x of
      (Term v) -> unfold (y v) state record
      _        -> let (x', effect) = unfold x state record
                  in  (Seq x' y, effect) 
    unfold (Par x y) state record = case (x,y) of
      (Term a , Term _) -> (Term a, NoEffect)
      (Term _ , _     ) -> unfold y state record
      (_      , Term _) -> unfold x state record
      (_      , _     ) ->
        let 
          (x', effectX) = unfold x state record
          (y', effectY) = unfold y state record
        in
          case (effectX, effectY) of
            (NoEffect   , NoEffect    ) -> (Par x' y', NoEffect   )
            (NoEffect   , Modified r  ) -> (Par x' y', Modified r )
            (Modified r , NoEffect    ) -> (Par x' y', Modified r )
            (Modified r , Modified s  ) -> (Par x' y', Modified (merge r s))
            (Apply r    , Apply s     ) -> (Par x' y', Apply (merge r s))
            (Apply _    , Modified s  ) -> unfold (Par x y') state s
            (Apply _    , NoEffect    ) -> unfold (Par x y') state record
            (Modified r , Apply _     ) -> unfold (Par x' y) state r
            (NoEffect   , Apply _     ) -> unfold (Par x' y) state record
    unfold (Preempt x y) state record = case (x,y) of
      (Term a , _     ) -> (Term a, NoEffect)
      (_      , Term b) -> (Term b, NoEffect)
      (_      , _     ) ->
        let 
          (x', effectX) = unfold x state record
          (y', effectY) = unfold y state record
        in
          case (effectX, effectY) of
            (NoEffect   , NoEffect    ) -> (Preempt x' y', NoEffect   )
            (NoEffect   , Modified r  ) -> (Preempt x' y', Modified r )
            (Modified r , NoEffect    ) -> (Preempt x' y', Modified r )
            (Modified r , Modified s  ) -> (Preempt x' y', Modified (merge r s))
            (Apply r    , Apply s     ) -> (Preempt x' y', Apply (merge r s))
            (Apply _    , Modified s  ) -> unfold (Preempt x y') state s
            (Apply _    , NoEffect    ) -> unfold (Preempt x y') state record
            (Modified r , Apply _     ) -> unfold (Preempt x' y) state r
            (NoEffect   , Apply _     ) -> unfold (Preempt x' y) state record