module LiSL.Record 
  ( carry
  , merge
  , toTargetState
  )
where

import qualified Data.HashMap.Strict as Map

import LiSL.Types


-- | carry the strictly forced parameters from @x@ to @y@ 
carry :: Record -> Record -> Record
carry x y = Map.foldlWithKey' updateY y x
  where
    updateY accmap k v = case (Map.member k accmap, v) of
      (False, Strict v1)  -> Map.insert k (Strict v1) accmap  
      (_    , _        )  -> accmap

-- | merge two records where `Lazy` has a greater precedence than `Strict`.
-- 
-- NOTE: elements in @x@ have a greater precedence than in @y@
merge :: Record -> Record -> Record
merge = Map.foldlWithKey' mergeXY
  where
    mergeXY accmap k v = case (accmap Map.!? k, v) of
      (Nothing        , _       ) -> Map.insert k v accmap
      (Just (Lazy _)  , _       ) -> accmap
      (_              , Lazy v2 ) -> Map.insert k (Lazy v2) accmap
      (_              , _       ) -> accmap

-- | convert a `Record` to a `TargetState`
toTargetState :: Record -> TargetState
toTargetState = fmap unpack
  where
    unpack (Strict x) = x
    unpack (Lazy x)   = x