
module LiSL.Parameter 
  ( Parameter(..)
  , Pack(..)
  )
where

import Data.Kind (Type)

import LiSL.Types


-- * Parameters

{-| Parameters implement the Haskell internal representation
of Record elements. A Parameter with a constructor of type Parameter a
tells the system that the Parameter with the name of the Constructor 
has the Type a. Available Types for `a` are `Bool, Int, Float`.

A Parameter type p with constructor @SomeParameterName :: p Int@ corresponds then to 
an element of the Record `{"SomeParameterName": 1}'. 

We use this dependently typed representation to enable typesafety in the
`Stimulation` monad, when we `LiSL.Stimulation.force` some parameter or `LiSL.Stimulation.get` a value.
-}
class Parameter (p :: Type -> Type) where
  paramKey :: p a -> String

-- | packing and unpacking of `Parameter` values
class Pack a where
  pack :: Parameter p => p a -> a -> Value
  unpack :: Parameter p => p a -> Value -> a
instance Pack Float where
  pack _ = Float
  unpack _ (Float v) = v
  unpack _ _ = error "unpack: type error"
instance Pack Int where
  pack _ = Int
  unpack _ (Int v) = v
  unpack _ _ = error "unpack: type error"
instance Pack Bool where
  pack _ = Bool
  unpack _ (Bool v) = v
  unpack _ _ = error "unpack: type error"
