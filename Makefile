

OUTFILE := CLiSL.so

BuildDir := build
DistDir := dist

OUT := $(DistDir)/$(OUTFILE)

HSCC := stack ghc --
HSFLAGS := --make -threaded -fPIC -flink-rts -dynamic -shared -odir $(BuildDir) -tmpdir $(BuildDir) -hidir $(BuildDir) -stubdir $(DistDir)
HSEXTS := -XExtendedDefaultRules -XDataKinds -XKindSignatures -XGADTs -XTypeOperators -XFlexibleContexts -XFlexibleInstances -XMultiParamTypeClasses -XForeignFunctionInterface -XStandaloneDeriving

HSSRC := $(shell find src -name "*.hs")
HSMAIN := app/*.hs

HSINIT := stack build --dependencies-only
HSCLEAN := stack clean

default: all

init:
	@mkdir -p $(BuildDir) $(DistDir) 2>&1 > /dev/null 
	@$(HSINIT)

clean:
	@$(RM) -r $(BuildDir)/*
	@$(RM) -r $(DistDir)/*
	@$(HSCLEAN)

all: CLiSL

CLiSL: init *.hs
	@$(HSCC) $(HSEXTS) $(HSFLAGS) $(HSSRC) $(HSMAIN) -o $(OUT)	

.PHONY: run	
run: all dist/*.so
	@LD_LIBRARY_PATH="$$(pwd)/dist" python try.py